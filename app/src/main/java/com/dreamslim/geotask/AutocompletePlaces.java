package com.dreamslim.geotask;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AutocompletePlaces {
    private static final String TAG = AutocompletePlaces.class.getSimpleName();
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyAAd5iTBMjRt55aDOD4hSpBKXFz7XJo3PI";

    public Map<String, ArrayList<String>> autocomplete (String input) {
        ArrayList<String> descriptionList = null;
        ArrayList<String> placeIdList = null;
        HttpURLConnection connection = null;
        StringBuilder jsonResults = new StringBuilder();
        Map<String, ArrayList<String>> resultMap = new HashMap<>();

        try {
            StringBuilder stringBuilder = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            stringBuilder.append("?key=" + API_KEY);
            stringBuilder.append("&input=").append(URLEncoder.encode(input, "utf8"));

            URL url = new URL(stringBuilder.toString());
            connection = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(connection.getInputStream());

            int read;
            char[] buffer = new char[1024];
            while ((read = in.read(buffer)) != -1) {
                jsonResults.append(buffer, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return null;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return null;
        } finally {
            if (connection != null)
                connection.disconnect();
        }

        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predictions = jsonObj.getJSONArray("predictions");

            descriptionList = new ArrayList<>(predictions.length());
            placeIdList = new ArrayList<>(predictions.length());
            for (int i = 0; i < predictions.length(); i++) {
                descriptionList.add(predictions.getJSONObject(i).getString("description"));
                placeIdList.add(predictions.getJSONObject(i).getString("place_id"));
            }
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }
        resultMap.put("description", descriptionList);
        resultMap.put("place_id", placeIdList);

        return resultMap;
    }
}
