package com.dreamslim.geotask.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dreamslim.geotask.AutocompletePlaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AutocompleteAdapter extends ArrayAdapter implements Filterable {
    Map<String, ArrayList<String>> resultMap = new HashMap<>();;
    ArrayList descriptionList;
    ArrayList placeIdList;
    Context mContext;
    int mResource;
    int mLayout;
    int mAutocompleteItem;
    AutocompletePlaces mAutocompletePlaces = new AutocompletePlaces();

    public AutocompleteAdapter(Context context, int resource, int layout, int autocompleteItem) {
        super(context, resource);
        mContext = context;
        mResource = resource;
        mLayout = layout;
        mAutocompleteItem = autocompleteItem;
    }

    @Override
    public int getCount() {
        return descriptionList.size();
    }

    @Override
    public Map<String, String> getItem(int position) {
        Map<String, String> item = new HashMap<>();

        item.put("description", descriptionList.get(position).toString());
        item.put("place_id", placeIdList.get(position).toString());
        return item;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);
        }
        TextView autocompleteText = ((TextView) convertView.findViewById(mAutocompleteItem));
        if (autocompleteText != null)
            autocompleteText.setText(descriptionList.get(position).toString());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    resultMap = mAutocompletePlaces.autocomplete(constraint.toString());
                    descriptionList = resultMap.get("description");
                    placeIdList = resultMap.get("place_id");

                    filterResults.values = descriptionList;
                    filterResults.count = descriptionList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }


}
