package com.dreamslim.geotask.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dreamslim.geotask.R;

import java.util.ArrayList;
import java.util.List;

public class GeoPagerAdapter extends FragmentPagerAdapter {

    private String[] tabTitles;
    private List<Fragment> mFragments = new ArrayList<>();

    public GeoPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        tabTitles = new String[] { context.getString(R.string.from), context.getString(R.string.to) };
    }

    public void addFragment(Fragment fragment) {
        mFragments.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
