package com.dreamslim.geotask;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.dreamslim.geotask.adapters.GeoPagerAdapter;
import com.dreamslim.geotask.fragments.FromFragment;
import com.dreamslim.geotask.fragments.ToFragment;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, FromFragment.OnPlaceSelectedListener, ToFragment.OnPlaceSelectedListener {
    private GoogleApiClient mGoogleApiClient;
    private static final int GOOGLE_API_CLIENT_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                .addApi(Places.GEO_DATA_API)
                .build();
        mGoogleApiClient.connect();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        GeoPagerAdapter geoPagerAdapter = new GeoPagerAdapter(getSupportFragmentManager(), this);
        geoPagerAdapter.addFragment(FromFragment.newInstance());
        geoPagerAdapter.addFragment(ToFragment.newInstance());
        viewPager.setAdapter(geoPagerAdapter);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onPlaceSelectFrom(String placeId) {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        final GoogleMap map = mapFragment.getMap();

        Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId)
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess()) {
                            final Place place = places.get(0);
                            LatLng selectedLocation = place.getLatLng();
                            map.addMarker(new MarkerOptions().position(selectedLocation).title("A"));
                            map.moveCamera(CameraUpdateFactory.newLatLng(selectedLocation));
                        }
                        places.release();
                    }
                });
    }

    @Override
    public void onPlaceSelectTo(String placeId) {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        final GoogleMap map = mapFragment.getMap();

        Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId)
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess()) {
                            final Place place = places.get(0);
                            LatLng selectedLocation = place.getLatLng();
                            map.addMarker(new MarkerOptions().position(selectedLocation).title("B"));
                            map.moveCamera(CameraUpdateFactory.newLatLng(selectedLocation));
                        }
                        places.release();
                    }
                });
    }

    @Override
    public void onMapReady(GoogleMap map) {
        LatLng moscow = new LatLng(55.76, 37.56);
        map.moveCamera(CameraUpdateFactory.newLatLng(moscow));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
