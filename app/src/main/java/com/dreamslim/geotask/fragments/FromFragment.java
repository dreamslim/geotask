package com.dreamslim.geotask.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.dreamslim.geotask.R;
import com.dreamslim.geotask.adapters.AutocompleteAdapter;

import java.util.HashMap;

public class FromFragment extends Fragment implements OnItemClickListener {
    String selectedPlaceId;
    OnPlaceSelectedListener onPlaceSelectedListener;
    AutoCompleteTextView autocompleteFrom;

    public interface OnPlaceSelectedListener {
        void onPlaceSelectFrom(String placeId);
    }

    public static FromFragment newInstance() {
        return new FromFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_from, container, false);

        autocompleteFrom = (AutoCompleteTextView) rootView.findViewById(R.id.autocompleteFrom);
        AutocompleteAdapter autocompleteAdapter = new AutocompleteAdapter(rootView.getContext(), R.layout.autocomplete_list, R.layout.fragment_from, R.id.autocompleteText);
        autocompleteFrom.setAdapter(autocompleteAdapter);
        autocompleteFrom.setOnItemClickListener(this);

        return rootView;
    }

    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        HashMap item = (HashMap) adapterView.getItemAtPosition(position);
        String selectedDescription = item.get("description").toString();
        autocompleteFrom.setText(selectedDescription);
        Toast.makeText(view.getContext(), selectedDescription, Toast.LENGTH_SHORT).show();
        selectedPlaceId = item.get("place_id").toString();
        onPlaceSelectedListener.onPlaceSelectFrom(selectedPlaceId);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            onPlaceSelectedListener = (OnPlaceSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
}
